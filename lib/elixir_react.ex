defmodule ElixirReact do
  use Application
  require Logger

  def start(_type, _args) do
    children = [
      {Plug.Adapters.Cowboy2, scheme: :http, plug: ElixirReact.MyPlug, option: [port: 8123]}

    ]

    Logger.info("Started Application")

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
